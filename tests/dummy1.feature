Feature: Sample CI 

	Very simple CI example with several 'dummy' linux shell commands


	Scenario: A student want to see CI on GitLab
		Given student attends to git course
		And student has access to gitlab.com
		When student navigates to the sample project
		And clicks on CI/CD menu
		Then student can see pipelines

